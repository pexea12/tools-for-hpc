#include <stdio.h>
#include <time.h>

#define N 1000


void reset(double C[N][N]) {
  int i, j;
  for (i = 0; i < N; ++i)
    for (j = 0; j < N; ++j)
      C[i][j] = 0;
}

int main()
{
  double A[N][N], B[N][N], C[N][N];
  double time1, time2, time3;
  clock_t start, end;

  int i, j, k;
  for (i = 0; i < N; ++i)
    for (j = 0; j < N; ++j) {
      A[i][j] = i * 2 + j;
      B[i][j] = i * 3 / (j + 0.2);
    }

  // C = AB
  reset(C);
  start = clock();
  for (i = 0; i < N; ++i)
    for (j = 0; j < N; ++j) {
      for (k = 0; k < N; ++k)
        C[i][j] += A[i][k] * B[k][j];
    }
  end = clock();

  for (i = 0; i < 10; ++i)
    printf("%f ", C[i][i]);
  printf("\n");

  time1 = (double) (end - start) / CLOCKS_PER_SEC;
  printf("Elapsed time (a): %f\n", time1);

  // C = A^T * B
  reset(C);
  start = clock();
  for (i = 0; i < N; ++i)
    for (j = 0; j < N; ++j) {
      for (k = 0; k < N; ++k)
        C[i][j] += A[k][i] * B[k][j];
    }
  end = clock();

  for (i = 0; i < 10; ++i)
    printf("%f ", C[i][i]);
  printf("\n");

  time2 = (double) (end - start) / CLOCKS_PER_SEC;
  printf("Elapsed time (b): %f\n", time2);

  // C = AB^T
  reset(C);
  start = clock();
  for (i = 0; i < N; ++i)
    for (j = 0; j < N; ++j) {
      for (k = 0; k < N; ++k)
        C[i][j] += A[i][k] * B[j][k];
    }
  end = clock();

  for (i = 0; i < 10; ++i)
    printf("%f ", C[i][i]);
  printf("\n");

  time3 = (double) (end - start) / CLOCKS_PER_SEC;
  printf("Elapsed time (c): %f\n", time3);

  return 0;
}
