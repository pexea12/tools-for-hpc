#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>


int main()
{
  int rc, ntasks, id;
  int i, number;

  rc = MPI_Init(NULL, NULL);
  if (rc != MPI_SUCCESS) {
    printf("MPI initialization failed\n");
    exit(1);
  }

  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (ntasks != 11) {
    printf("This program must run with exactly 11 processors\n");
    exit(1);
  }

  if (id == 0) {
    // Receive 100 messages
    for (i = 0; i < 100; ++i) {
      MPI_Status status;
      MPI_Recv(
        &number,
        1,
        MPI_INT,
        MPI_ANY_SOURCE,
        MPI_ANY_TAG,
        MPI_COMM_WORLD,
        &status
      );

      printf("Receive 0 <- %d: %d, Tag = %d\n", status.MPI_SOURCE, number, status.MPI_TAG);
    }
  } else {
    // Each processor sends 10 messages to processor 0
    // Tag == id

    srand(time(NULL) + id * 10);
    for (i = 0; i < 10; ++i) {
      number = rand();
      MPI_Send(
          &number,
        1,
        MPI_INT,
        0, // Send to processor 0
        id, // Tag == id
        MPI_COMM_WORLD
      );

      printf("Send %d -> 0: %d\n", id, number);
    }
  }

  MPI_Finalize();
  return 0;
}
