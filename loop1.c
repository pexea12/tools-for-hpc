#include <stdio.h>
#include <time.h>

#define N 10000000


int main()
{
  double a[N], b[N];
  clock_t start, end;

  for (int i = 0; i < N; ++i) b[i] = i;

  int i;

  start = clock();
  for (i = 0; i < N - 1; ++i) {
    if (i < 500)
      a[i] = 4.0 * b[i] + b[i + 1];
    else
      a[i] = 4.0 * b[i + 1] + b[i];
  }

  end = clock();

  for (int i = 0; i < 100; ++i) {
    printf("%f\n", a[i]);
  }

  double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("Elapsed time: %f\n", cpu_time_used);

  return 0;
}
