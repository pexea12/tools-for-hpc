#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

#define TOTAL_EXCHANGE 5
#define SEED 1
#define N 10000000
#define TAG 100
struct timeval timecheck;

long get_current_time() {
  gettimeofday(&timecheck, NULL);
  return (long) timecheck.tv_sec * 1000 + (long) timecheck.tv_usec / 1000;
}

void fill(long* a, int n) {
  int i;
  for (int i = 0; i < n; ++i)
    a[i] = cos(rand());
}

int main(int argc, char *argv[])
{
  int rc;
  rc = MPI_Init(&argc, &argv);
  if (rc != MPI_SUCCESS) {
    printf("MPI initialization failed\n");
    exit(1); } int ntasks, id;
  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (ntasks != 2) {
    printf("Must run with only 2 processors\n");
    exit(1);
  }

  srand(5000 + id * 100);

  if (id == 0) {
    int opponent_id = 1;
    int i;
    long *a = (long*) malloc(N * sizeof(long));
    for (i = 0; i < TOTAL_EXCHANGE; ++i) {
      // Send
      fill(a, N);
      long send_time = get_current_time();
      a[0] = send_time;
      MPI_Send(a, N, MPI_LONG, opponent_id, TAG, MPI_COMM_WORLD);

      printf("Ping sends ball %ld to pong at %ld.\n", a[0], send_time);

      // Receive
      MPI_Recv(a, N, MPI_LONG, opponent_id, TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      long receive_time = get_current_time();
      double wctime = (double) (receive_time - a[0]) / 1000;
      printf("Ping receives ball %ld from pong at %ld after %.3f seconds.\n", a[0], receive_time, wctime);
    }

  } else {
    int opponent_id = 0;
    int i;
    long *a = (long*) malloc (N * sizeof(long));
    for (i = 0; i < TOTAL_EXCHANGE; ++i) {
      // Receive
      MPI_Recv(a, N, MPI_LONG, opponent_id, TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      long receive_time = get_current_time();
      double wctime = (double) (receive_time - a[0]) / 1000;
      printf("Pong receives ball %ld from ping at %ld after %.3f seconds.\n", a[i], receive_time, wctime);

      // Send
      fill(a, N);
      long send_time = get_current_time();
      a[0] = send_time;
      MPI_Send(a, N, MPI_LONG, opponent_id, TAG, MPI_COMM_WORLD);

      printf("Pong sends ball %ld to ping at %ld\n", a[0], send_time);
    }
  }

  MPI_Finalize();


  return 0;
}
