#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(int argc, char *argv[])
{
  FILE *file;
  int N, i;
  double number;
  long double sum = 0.0;
  long double sum_squares = 0.0;
  long double mean, variance;

  srand(time(0));
  N = atoi(argv[1]);

  file = fopen("numbers.bin", "wb");
  fwrite((void*) &N, sizeof(N), 1, file);
  for (i = 0; i < N; ++i) {
    number = (double) rand() / 31;
    sum += number;
    sum_squares += number * number;
    fwrite((void*) &number, sizeof(number), 1, file);
  }

  fclose(file);

  mean = sum / N;
  variance = sum_squares / N - mean * mean;

  printf("Mean = %.10Lf\n", mean);
  printf("Variance = %.10Lf\n", variance);

  return 0;
}
