#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <mpi.h>


int main(int argc, char *argv[])
{
  int N = atoi(argv[1]);
  double x, y, estimated_pi;
  int ntasks, rc, id;
  int total_inside, count_inside = 0;
  int source_id;

  rc = MPI_Init(NULL, NULL);
  if (rc != MPI_SUCCESS) {
    printf("MPI initialization failed\n");
    exit(1);
  }

  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  srand(time(NULL) + id * 10); // Different seed for each processor
  for (int i = 0; i < N; ++i) {
    x = (double) rand() / RAND_MAX;
    y = (double) rand() / RAND_MAX;
    if (x * x + y * y <= 1) ++count_inside;
  }

  /**
    Processor 0 is the reducer
    Processor 1..n send the result to processor 0
    */
  if (id == 0) {
    total_inside = count_inside;
    printf("Processor %d gets %d / %d\n", id, count_inside, N);

    // Receive count_inside from other processors
    for (source_id = 1; source_id < ntasks; ++source_id) {
      MPI_Recv(
        &count_inside,
        1, // Receive 1 number
        MPI_INT,
        source_id,
        0, // Tag == 0
        MPI_COMM_WORLD,
        MPI_STATUS_IGNORE
      );

      printf("Receive %d / %d from processor %d\n", count_inside, N, source_id);
      total_inside += count_inside;
    }

    estimated_pi = (double) count_inside / N * 4;
    printf("Estimated pi: %.10f\n", estimated_pi);
  } else {
    MPI_Send(
      &count_inside,
      1, // Send 1 number
      MPI_INT,
      0, // ID or reducer
      0, // Tag == 0
      MPI_COMM_WORLD
    );
  }

  MPI_Finalize();
  return 0;
}
