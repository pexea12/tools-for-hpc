#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


int main(int argv, char *argc[])
{
  int rc, ntasks, id;
  int N, work_load, processor_id;
  int i;
  long double sum = 0.0;
  long double sum_squares = 0.0;
  FILE *file;

  rc = MPI_Init(NULL, NULL);
  if (rc != MPI_SUCCESS) {
    printf("MPI Initialization failed\n");
    exit(1);
  }

  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (id == 0) {
    // Read N from file
    file = fopen("numbers.bin", "rb");
    fread((void*) &N, sizeof(N), 1, file);

    // Determine work_load for each processor
    work_load = N / ntasks;

    printf("N = %d, work_load = %d\n", N, work_load);

    // Each processor processes (work_load) numbers from file
    // Send work_load to each processor
    for (processor_id = 1; processor_id < ntasks; ++processor_id) {
      MPI_Send(
        &work_load,
        1, // Send 1 number
        MPI_INT,
        processor_id, // receiver
        0, // Tag == 0
        MPI_COMM_WORLD
      );
    }

    // Processor 0 take (work_load) numbers and the remaining
    work_load += N % ntasks;

    // Processor 0 reads (work_load) last number
    double *numbers = (double*) malloc(work_load * sizeof(double));

    // Move cursor to the last (work_load) bytes
    // (Recall that numbers.bin start with an int and then N doubles)
    fseek(file, -sizeof(double) * work_load, SEEK_END);
    fread(numbers, sizeof(double), work_load, file);

    fclose(file);

    // Calculate sum and sum_squares
    for (i = 0; i < work_load; ++i) {
      sum += numbers[i];
      sum_squares += numbers[i] * numbers[i];
    }

    free(numbers);

    printf("Processor %d gets sum = %Lf, sum_squares = %Lf\n", id, sum, sum_squares);
    // Receive sum and sum_squares from other processors
    long double results[2];
    for (processor_id = 1; processor_id < ntasks; ++processor_id) {
      MPI_Recv(
        &results,
        2, // Receive 2 number
        MPI_LONG_DOUBLE,
        processor_id, // Receive from processor_id
        1, // Tag == 1
        MPI_COMM_WORLD,
        MPI_STATUS_IGNORE
      );

      printf("Receive sum = %Lf, sum_squares = %Lf from processor %d\n", results[0], results[1], processor_id);

      sum += results[0];
      sum_squares += results[1];
    }

    long double mean = sum / N;
    long double variance = sum_squares / N - mean * mean;

    printf("Mean = %Lf\n", mean);
    printf("Variance = %Lf\n", variance);
  } else {
    // Each processor receives its number of work_load
    MPI_Recv(
      &work_load,
      1, // Receive 1 number
      MPI_INT,
      0, // Receive from processor 0
      0, // Tag == 0
      MPI_COMM_WORLD,
      MPI_STATUS_IGNORE
    );

    // Processor id reads (work_load) numbers from
    // byte [sizeof(int) + sizeof(double) * (id - 1) * work_load]
    // (Recall that numbers.bin start with an int and then N doubles)
    file = fopen("numbers.bin", "rb");

    double *numbers = (double*) malloc(work_load * sizeof(double));

    // Move cursor to byte [sizeof(int) + sizeof(double) * (id - 1)]*/
    fseek(file, sizeof(int) + sizeof(double) * work_load * (id - 1), SEEK_SET);
    fread(numbers, sizeof(double), work_load, file);
    fclose(file);

    // Calculate sum and sum_squares
    for (i = 0; i < work_load; ++i) {
      sum += numbers[i];
      sum_squares += numbers[i] * numbers[i];
    }

    free(numbers);

    // Send back to processor 0 to combine
    long double results[2];
    results[0] = sum;
    results[1] = sum_squares;

    MPI_Send(
      &results,
      2,
      MPI_LONG_DOUBLE,
      0, // Send to processor 0
      1, // Tag == 1
      MPI_COMM_WORLD
    );
  }

  MPI_Finalize();
  return 0;
}
