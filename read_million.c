#include <stdio.h>

#define N 1000

int main()
{
  double a[N];
  int i;
  FILE *fu;

  fu = fopen("uniform_formatted.datc", "r");
  for (i = 0; i < N; ++i) {
    //fread((void *)&a[i], sizeof(a[i]), 1, fu);
    fscanf(fu, "%lf", &a[i]);
  }

  for (i = 1000; i < 1010; ++i) {
    printf("%d %f\n", i, a[i]);
  }

  return 0;
}
