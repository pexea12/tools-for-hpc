#include <stdio.h>


int ackermann(int m, int n) {
  if (m == 0) return n + 1;
  if (m > 0 && n == 0) return ackermann(m - 1, 1);
  return ackermann(m - 1, ackermann(m, n - 1));
}

int main()
{
  int m, n;
  printf("m = "); scanf("%d", &m);
  printf("n = "); scanf("%d", &n);

  printf("\nackermann(%d, %d) = %d\n", m, n, ackermann(m, n));
  return 0;
}
