#include <stdio.h>
#include <time.h>

#define N 400000000

int main()
{
  double A[N], B[N];
  clock_t start, end;
  int i;

  for (i = 0; i < N; ++i) {
    B[i] = i + 0.3;
  }

  start = clock();
  for (i = 0; i < N; ++i) {
    A[i] = B[i] / (i % 999983 + 0.03);
  }
  end = clock();

  for (i = 0; i < 10; ++i)
    printf("%f ", A[i]);
  printf("\n");

  double elapsed_time = (double) (end - start) / CLOCKS_PER_SEC;

  printf("CPU time: %f\n", elapsed_time);

  return 0;
}
