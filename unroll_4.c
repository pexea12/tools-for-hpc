#include <stdio.h>
#include <math.h>
#include <time.h>

#define N 400000000

int main()
{
  double A[N], B[N];
  clock_t start, end;
  int i;

  for (i = 0; i < N; ++i) {
    B[i] = i + 0.3;
  }

  start = clock();
  for (i = 0; i < N - 3; i += 4) {
    A[i] = B[i] / (i % 999983 + 0.03);
    A[i + 1] = B[i + 1] / ((i + 1) % 999983 + 0.03);
    A[i + 2] = B[i + 2] / ((i + 2) % 999983 + 0.03);
    A[i + 3] = B[i + 3] / ((i + 3) % 999983 + 0.03);
  }
  end = clock();

  for (i = 0; i < 10; ++i)
    printf("%f ", A[i]);
  printf("\n");

  double elapsed_time = (double) (end - start) / CLOCKS_PER_SEC;

  printf("CPU time: %f\n", elapsed_time);

  return 0;
}
