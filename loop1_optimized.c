#include <stdio.h>
#include <time.h>

#define N 10000000


int main()
{
  double a[N], b[N];
  clock_t start, end;

  for (int i = 0; i < N; ++i) b[i] = i;

  int i;

  start = clock();
  for (i = 0; i < 500; i += 2) {
    a[i] = 4.0 * b[i] + b[i + 1];
    a[i + 1] = 4.0 * b[i + 1] + b[i + 2];
  }

  for (i = 500; i < N - 2; i += 2) {
    a[i] = 4.0 * b[i + 1] + b[i];
    a[i + 1] = 4.0 * b[i + 2] + b[i + 1];
  }

  a[N - 2] = 4.0 * b[N - 1] + b[N - 2];

  end = clock();

  for (int i = 0; i < 100; ++i) {
    printf("%f\n", a[i]);
  }

  double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("Elapsed time: %f\n", cpu_time_used);

  return 0;
}
