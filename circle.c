#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
  int rc;
  rc = MPI_Init(&argc, &argv);
  if (rc != MPI_SUCCESS) {
    printf("MPI initialization failed\n");
    exit(1);
  }

  int ntasks, id;
  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (ntasks < 2) {
    printf("Must run with more than 2 processors\n");
    exit(1);
  }

  srand(id + time(NULL)); // Seed differently

  int message[3];
  message[0] = id; // sender
  message[1] = id + 1; // receiver
  message[2] = rand(); // random number


  if (id < ntasks - 1) {
    MPI_Send(
      message,
      3,
      MPI_INT,
      id + 1,
      100, // tag
      MPI_COMM_WORLD
    );
    printf("Sender %d -> Receiver %d with message(%d, %d, %d)\n", id, id + 1, message[0], message[1], message[2]);
  }

  if (id > 0) {
    MPI_Recv(
      message,
      3,
      MPI_INT,
      id - 1,
      100, // tag
      MPI_COMM_WORLD,
      MPI_STATUS_IGNORE
    );

    printf("Receiver %d <- Sender %d with message(%d, %d, %d)\n", id, id - 1, message[0], message[1], message[2]);
  }

  MPI_Finalize();

  return 0;
}
