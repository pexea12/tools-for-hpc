#include <stdio.h>
#include <time.h>

#define N 3000


int main()
{
  double a[N][N], b[N][N], c[N];
  clock_t start, end;


  for (int i = 0; i < N; ++i) {
    c[i] = i + 0.1;
    for (int j = 0; j < N; ++j) b[i][j] = i * 2.0 + j;
  }

  start = clock();
  for (int i = 0; i < N; i += 2) {
    for (int j = 0; j < N; j += 2) {
      a[i][j] = b[i][j] / c[i];
      a[i][j + 1] = b[i][j + 1] / c[i];
      a[i + 1][j] = b[i + 1][j] / c[i + 1];
      a[i + 1][j + 1] = b[i + 1][j + 1] / c[i + 1];
    }
  }

  end = clock();

  for (int i = 0; i < 20; ++i)
    for (int j = 0; j < 20; ++j)
      printf("%f\n", a[i][j]);

  double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("Elapsed time: %f\n", cpu_time_used);

  return 0;
}
