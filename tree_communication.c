#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


int main()
{
  int rc, ntasks, id;
  int element;

  rc = MPI_Init(NULL, NULL) ;
  if (rc != MPI_SUCCESS) {
    printf("MPI initialization failed\n");
    exit(1);
  }

  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (ntasks == 0 || (ntasks & (ntasks - 1)) != 0) {
    printf("The number of processors must be a power of 2. Found %d processors\n", ntasks);
    exit(1);
  }

  // Assume that processor id will have an initial value of (id)
  // The total sum should be ntasks * (ntasks - 1) / 2
  int sum = id;

  if (id == 0) {
    int source_id;

    // Processor 0 receives from processor 2^n
    for (source_id = 1; source_id < ntasks; source_id *= 2) {
      MPI_Recv(
        &element,
        1, // Receive 1 number
        MPI_INT,
        source_id,
        0, // Tag == 0
        MPI_COMM_WORLD,
        MPI_STATUS_IGNORE
      );

      sum += element;
      printf("PROCESSOR 0 receives %d from processor %d\n", element, source_id);
    }

    printf("\nFinal Sum = %d\n", sum);
	} else {
    // Processor id = 2^k * p (p is odd) receives from processor (id + 2^j)
    // 0 <= j < k

    // Find 2^j and p
    // power_j = 2^j
    int power_j = 1, p = id;
    while (p % 2 == 0 && (id + power_j < ntasks)) {
      MPI_Recv(
        &element,
        1, // Receive 1 number
        MPI_INT,
        id + power_j,
        0, // Tag == 0
        MPI_COMM_WORLD,
        MPI_STATUS_IGNORE
      );

      printf("Receive: %d <- %d: value %d\n", id, id + power_j, element);

      sum += element;
      power_j *= 2;
      p /= 2;
    }

    // Processor id = 2^k * p (p is odd) sends to processor (2^k * (p - 1))
    // Now power_j = 2^k
    int receiver = power_j * (p - 1);
    MPI_Send(
      &sum,
      1, // Send 1 number
      MPI_INT,
      receiver, // Send to (2^k * (p - 1))
      0, // Tag == 0
      MPI_COMM_WORLD
    );

    printf("Send: %d -> %d: value %d\n", id, receiver, sum);
  }

  MPI_Finalize();
  return 0;
}
