#include <stdio.h>
#include <time.h>
#include <omp.h>

#define N 47
#define NUM_THREADS 4


int fib(int k) {
  if (k <= 1) return k;
  return fib(k - 1) + fib(k - 2);
}

int main()
{
  long a[N];
  int i;
  clock_t cpu_start, cpu_end;
  double start, end;

  omp_set_num_threads(NUM_THREADS);

  cpu_start = clock();
  start = omp_get_wtime();

  #pragma omp parallel for schedule(dynamic)
  for (i = 0; i < N; ++i) {
    a[i] = fib(i);
  }

  long long sum = 0;
  #pragma omp parallel for reduction (+:sum)
  for (i = 0; i < N; ++i) {
    sum += a[i];
  }

  cpu_end = clock();
  end = omp_get_wtime();

  printf("Sum: %lld\n", sum);

  double cpu_time = (double) (cpu_end - cpu_start) / CLOCKS_PER_SEC;
  double wall_time = end - start;
  printf("CPU Time: %f\n", cpu_time);
  printf("Wall-clock Time: %f\n", wall_time);

  return 0;
}