#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  int rc;
  rc = MPI_Init(&argc, &argv);
  if (rc != MPI_SUCCESS) {
    printf("MPI initialization failed\n");
    exit(1);
  }

  int ntasks, id;
  MPI_Comm_size(MPI_COMM_WORLD, &ntasks);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (ntasks != 2) {
    printf("Must run with only 2 processors\n");
    exit(1);
  }

  int track = 100;
  int tag = 50;
  if (id == 0) {
    int opponent_id = 1;
    int i;

    for (i = 0; i < 2; ++i) {
      ++track;
      MPI_Send(&track, 1, MPI_INT, opponent_id, tag, MPI_COMM_WORLD);
      printf("Ping sends ball %d to pong\n", track);

      MPI_Recv(&track, 1, MPI_INT, opponent_id, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      printf("Ping receives ball %d from pong\n", track);
    }

  } else {
    int opponent_id = 0;
    int i;

    for (i = 0; i < 2; ++i) {
      MPI_Recv(&track, 1, MPI_INT, opponent_id, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      printf("Pong receives ball %d from ping\n", track);

      ++track;
      MPI_Send(&track, 1, MPI_INT, opponent_id, tag, MPI_COMM_WORLD);
      printf("Pong sends ball %d to ping\n", track);
    }
  }

  MPI_Finalize();

  printf("\nBall final value: %d\n", track);

  return 0;
}
