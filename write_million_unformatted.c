#include <stdio.h>
#include <time.h>

#define N 10000000


int main()
{
  double a[N];
  clock_t start_time, end_time;
  FILE *fu;
  int i;

  for (i = 0; i < N; ++i) {
    a[i] = (i + 5.8) / 39.12;
  }

  printf("Start writing file...\n");
  start_time = clock();
  fu = fopen("uniform_unformatted.datc", "w");
  for (i = 0; i < N; ++i) {
    fwrite((void *)&a[i], sizeof(a[i]), 1, fu);
  }

  end_time = clock();

  fclose(fu);
  double cpu_time = (double) (end_time - start_time) / CLOCKS_PER_SEC;
  printf("CPU Time: %.5f\n", cpu_time);

  return 0;
}
