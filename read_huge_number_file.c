#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
  FILE *file;
  int N, i;
  double number;

  file = fopen("numbers.bin", "rb");
  fread((void*) &N, sizeof(N), 1, file);
  printf("%d\n", N);
  //for (i = 0; i < N + 1; ++i) {
  //  fread((void*) &number, sizeof(number), 1, file);
  //  printf("%.5f\n", number);
  //}
  
  double *numbers = (double*) malloc(sizeof(double) * N);
  fread(numbers, sizeof(double), N, file);

  for (i = 0; i < N; ++i) {
    printf("%.5f\n", numbers[i]);
  } 

  fclose(file);
  return 0;
}
