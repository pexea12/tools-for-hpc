#include <stdio.h>
#include <math.h>
#include <time.h>

#define N 400000000

int main()
{
  double A[N], B[N];
  clock_t start, end;
  int i;

  for (i = 0; i < N; ++i) {
    B[i] = i + 0.3;
  }

  start = clock();
  for (i = 0; i < N - 15; i += 16) {
    A[i] = B[i] / (i % 999983 + 0.03);
    A[i + 1] = B[i + 1] / ((i + 1) % 999983 + 0.03);
    A[i + 2] = B[i + 2] / ((i + 2) % 999983 + 0.03);
    A[i + 3] = B[i + 3] / ((i + 3) % 999983 + 0.03);
    A[i + 4] = B[i + 4] / ((i + 4) % 999983 + 0.03);
    A[i + 5] = B[i + 5] / ((i + 5) % 999983 + 0.03);
    A[i + 6] = B[i + 6] / ((i + 6) % 999983 + 0.03);
    A[i + 7] = B[i + 7] / ((i + 7) % 999983 + 0.03);
    A[i + 8] = B[i + 8] / ((i + 8) % 999983 + 0.03);
    A[i + 9] = B[i + 9] / ((i + 9) % 999983 + 0.03);
    A[i + 10] = B[i + 10] / ((i + 10) % 999983 + 0.03);
    A[i + 11] = B[i + 11] / ((i + 11) % 999983 + 0.03);
    A[i + 12] = B[i + 12] / ((i + 12) % 999983 + 0.03);
    A[i + 13] = B[i + 13] / ((i + 13) % 999983 + 0.03);
    A[i + 14] = B[i + 14] / ((i + 14) % 999983 + 0.03);
    A[i + 15] = B[i + 15] / ((i + 15) % 999983 + 0.03);
  }
  end = clock();

  for (i = 0; i < 10; ++i)
    printf("%f ", A[i]);
  printf("\n");

  double elapsed_time = (double) (end - start) / CLOCKS_PER_SEC;

  printf("CPU time: %f\n", elapsed_time);

  return 0;
}
