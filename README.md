# Tools for High Performance Computing

Courses: MATR326 Tools of High Performance Computing (University of Helsinki)

- `exercises`: Exercise
- `solutions`: Solution

This repository uses C and OpenMP for parallelism to demonstrate the examples.
