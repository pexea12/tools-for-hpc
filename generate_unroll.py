import sys


n = int(sys.argv[1])

template = """
#include <stdio.h>
#include <time.h>

#define N 400000000

int main()
{
  double A[N], B[N];
  clock_t start, end;
  int i;

  for (i = 0; i < N; ++i) {
    B[i] = i + 0.3;
  }

  start = clock();
"""

if n == 0:
    template += '  for (i = 0; i < N; ++i) {\n'
else:
    template += '  for (i = 0; i < N - %d; i += %d) {\n' % (n - 1, n)

template += '    A[i] = B[i] / (i % 999983 + 0.03);\n'
for i in range(n - 1):
    template += '    A[i + %d] = B[i + %d] / ((i + %d) %% 999983 + 0.03);\n' % (i + 1, i + 1, i + 1)

template += """
  }
  end = clock();

  for (i = 0; i < 10; ++i)
    printf("%f ", A[i]);
  printf("\\n");

  double elapsed_time = (double) (end - start) / CLOCKS_PER_SEC;

  printf("CPU time: %f\\n", elapsed_time);

  return 0;
}
"""

with open('unroll_' + str(n) + '.c', 'w') as f:
    f.write(template)
